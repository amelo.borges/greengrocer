import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/base/base_binding.dart';
import 'package:greengrocer/presentation/modules/sign-in/sign_in_binding.dart';
import 'package:greengrocer/presentation/routes/app_pages.dart';
import 'package:greengrocer/presentation/routes/app_routes.dart';

void main() {
  runApp(const GreenGrocer());
}

class GreenGrocer extends StatefulWidget {
  const GreenGrocer({Key? key}) : super(key: key);

  @override
  State<GreenGrocer> createState() => _GreenGrocerState();
}

class _GreenGrocerState extends State<GreenGrocer> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green
      ),
      initialRoute: AppRoutes.base,
      initialBinding: BaseBinding(),
      getPages: AppPages.pages
    );
  }
}

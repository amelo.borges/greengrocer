import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/sign-in/sign_in_controller.dart';
import 'package:greengrocer/presentation/widgets/custom_text_field.dart';

class SignInView extends StatelessWidget {
  const SignInView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final device = MediaQuery.of(context);
    return GetBuilder<SignInController>(
      builder: (ctrl) {
        return Scaffold(
          backgroundColor: Colors.green,
          body: SingleChildScrollView(
            child: SizedBox(
              height: device.size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text.rich(
                          TextSpan(
                            style: TextStyle(
                              fontSize: 40.0
                            ),
                            children: [
                              TextSpan(
                                text: "Green",
                                style: TextStyle(
                                  color: Colors.white
                                )
                              ),
                              TextSpan(
                                  text: "grocer",
                                style: TextStyle(
                                  color: Colors.red
                                )
                              )
                            ]
                          ),
                        ),
                        SizedBox(
                          height: 40.0,
                          child: DefaultTextStyle(
                            style: const TextStyle(
                              fontSize: 25.0
                            ),
                            child: AnimatedTextKit(
                                repeatForever: true,
                                pause: const Duration(microseconds: 5),
                                animatedTexts: [
                                  FadeAnimatedText("Carnes"),
                                  FadeAnimatedText("Verduras"),
                                  FadeAnimatedText("Legumes"),
                                  FadeAnimatedText("Frios"),
                                  FadeAnimatedText("Bebidas"),
                                  FadeAnimatedText("Laticínios"),
                                  FadeAnimatedText("Higiene"),
                                  FadeAnimatedText("Mercearia"),
                                ]),
                          ),
                        )
                      ],
                    )
                  ),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.only(
                        left: 16.0, top: 40.0, right: 16.0, bottom: 50.0),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25.0),
                        topRight: Radius.circular(25.0),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        CustomTextField(
                          label: "E-mail",
                          icon: const Icon(Icons.email),
                          changedInputText: ctrl.changedInputEmail,
                        ),
                        CustomTextField(
                          label: "Password",
                          icon: const Icon(Icons.lock),
                          changedInputText: ctrl.changedInputEmail,
                          isSecret: true,
                        ),
                        SizedBox(
                          height: 45.0,
                          child: ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                            child: const Text("Entrar"),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: TextButton(
                            onPressed: () {},
                            child: const Text("Esqueceu a senha"),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Divider(
                                  thickness: 2.0,
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.0),
                                child: Text("Ou"),
                              ),
                              Expanded(
                                child: Divider(
                                  thickness: 2.0,
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 45.0,
                          child: OutlinedButton(
                            onPressed: () {},
                            style: OutlinedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              side:
                                  const BorderSide(color: Colors.green, width: 2.0),
                            ),
                            child: const Text("Criar conta"),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

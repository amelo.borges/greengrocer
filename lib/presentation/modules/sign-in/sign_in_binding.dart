import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/sign-in/sign_in_controller.dart';

class SignInBinding extends Bindings{


  @override
  void dependencies() {
    Get.lazyPut(() => SignInController());
  }

}
import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/base/base_controller.dart';

class BaseBinding extends Bindings{

  @override
  void dependencies() {
    Get.lazyPut(() => BaseController());
  }

}
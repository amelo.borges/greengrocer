import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/base/base_controller.dart';

class BaseView extends StatelessWidget {
  const BaseView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pageController = PageController();
    return GetBuilder<BaseController>(
        builder: (ctrl) {
          return Scaffold(
            appBar: AppBar(
              title: const Text("Greengrocer"),
            ),
            body: PageView(
              controller: pageController,
              padEnds: true,
              pageSnapping: true,
              children: [
                Container(color: Colors.cyan),
                Container(color: Colors.red),
                Container(color: Colors.blue),
                Container(color: Colors.amber),
              ],
              onPageChanged: (index){
                ctrl.tabBottomNavigation(index);
              },
            ),
            bottomNavigationBar: Obx(() {
              return BottomNavigationBar(
                currentIndex: ctrl.currentIndex.value,
                type: BottomNavigationBarType.fixed,
                backgroundColor: Colors.green,
                selectedItemColor: Colors.white,
                unselectedItemColor: Colors.white.withOpacity(.5),
                onTap: (index) {
                  ctrl.currentIndex.value = index;
                  pageController.jumpToPage(index);
                },
                items: const [
                  BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: "Home"),
                  BottomNavigationBarItem(icon: Icon(Icons.shopping_cart_outlined), label: "Carrinho"),
                  BottomNavigationBarItem(icon: Icon(Icons.list), label: "Pedidos"),
                  BottomNavigationBarItem(icon: Icon(Icons.person_outline), label: "Perfil"),
                ],
              );
            },),
          );
        }
    );
  }
}

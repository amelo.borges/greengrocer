import 'package:get/get.dart';
import 'package:greengrocer/presentation/modules/base/base_binding.dart';
import 'package:greengrocer/presentation/modules/base/base_view.dart';
import 'package:greengrocer/presentation/modules/sign-in/sign_in_binding.dart';
import 'package:greengrocer/presentation/modules/sign-in/sign_in_view.dart';
import 'package:greengrocer/presentation/routes/app_routes.dart';

class AppPages{
  static List<GetPage> pages = [
    GetPage(
        transition: Transition.cupertino,
        name: AppRoutes.base,
        page: () => const BaseView(),
        binding: BaseBinding()
    ),
    GetPage(
        transition: Transition.cupertino,
        name: AppRoutes.signIn,
        page: () => const SignInView(),
        binding: SignInBinding()
    )
  ];
}
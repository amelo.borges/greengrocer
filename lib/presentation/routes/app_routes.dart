class AppRoutes {
  static const String base = "/base";
  static const String home = "/home";
  static const String signIn = "/signIn";
}